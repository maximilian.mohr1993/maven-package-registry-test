package de.mohr.mavenpackageregistrytest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MainClassTest {

    private MainClass main;

    @BeforeEach
    void setup() {
        main = new MainClass();
    }

    @Test
    void whenMain_thenReturnHelloWorldString() {
        final String result = main.main();
        assertEquals(result, "Hello World!");
    }
}
